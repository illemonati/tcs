import { NavigationContainer } from '@react-navigation/native';
import LoginPage from './pages/Login';
import { Provider as PaperProvider } from 'react-native-paper';
import { StyleSheet, View } from 'react-native';

export default function App() {
    return (
        <NavigationContainer>
            <PaperProvider>
                <View style={styles.app}>
                    <LoginPage />
                </View>
            </PaperProvider>
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    app: {
        flex: 1,
        backgroundColor: '#FFF',
    },
});
