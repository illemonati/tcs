import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import { Text, TextInput } from 'react-native-paper';
import Login from '../components/Login';

const LoginPage: React.FC = () => {
    return (
        <View style={styles.container}>
            <View style={styles.title}>
                <Text variant="headlineLarge">TCS</Text>
                <Text variant="labelLarge">A Chat Messager Created by STIC at VT</Text>
            </View>
            <Login />
            <StatusBar style="auto" />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingTop: '20%',
        // backgroundColor: '#fff',
        alignItems: 'center',
        height: '100%',
        width: '100%',
    },
    title: {
        alignItems: 'center',
        marginBottom: '8%',
    },
});

export default LoginPage;
