import { sha3_256 } from 'js-sha3';
import { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Text, TextInput } from 'react-native-paper';

const Login: React.FC = () => {
    const [username, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [hash, setHash] = useState('');

    useEffect(() => {
        if (!username || !password) {
            setHash('');
            return;
        }
        const combined = JSON.stringify({
            username: username,
            password: password,
        });
        setHash(sha3_256(combined));
    }, [username, password]);

    return (
        <View style={styles.container}>
            <View style={styles.inputBlock}>
                <Text variant="labelLarge">Username</Text>
                <TextInput
                    value={username}
                    onChangeText={setUserName}
                    style={styles.input}
                    textContentType="username"
                    mode="outlined"
                    placeholder="Enter your username"
                />
            </View>
            <View style={styles.inputBlock}>
                <Text variant="labelLarge">Password</Text>
                <TextInput
                    value={password}
                    onChangeText={setPassword}
                    style={styles.input}
                    textContentType="password"
                    secureTextEntry
                    mode="outlined"
                    placeholder="Enter your password"
                />
            </View>
            <View style={styles.centered}>
                <Text variant="labelLarge">
                    <Text>{hash}</Text>
                </Text>
            </View>

            <View style={styles.loginButtonContainer}>
                <Button disabled={!hash} mode="contained">
                    Login
                </Button>
            </View>
            <View style={styles.centered}>
                <Text variant="labelLarge">Registeration is not required, simply log in.</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '80%',
        height: '20%',
        alignItems: 'flex-start',
        backgroundColor: '#FFF',
    },
    input: {
        width: '100%',
    },
    inputBlock: {
        width: '100%',
        marginBottom: '5%',
    },
    loginButtonContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '5%',
        marginBottom: '5%',
    },
    centered: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default Login;
